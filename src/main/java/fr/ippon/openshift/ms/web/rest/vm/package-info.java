/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.ippon.openshift.ms.web.rest.vm;
